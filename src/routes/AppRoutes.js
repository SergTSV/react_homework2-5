import React from 'react';
import Favorites from "../page/Favorites/Favorites";
import { Routes, Route } from 'react-router-dom'
import CartPage from "../page/Cart/CartPage";
import Main from "../page/Main/Main";



const AppRoutes = () => {

  return (
    <>
      <Routes>
        <Route path="/" element={<Main />} />
        <Route path="/cart" element={<CartPage />} />
        <Route path="/favorites" element={<Favorites />} />}/>
        <Route path="/*" element={<Main />} />
      </Routes>
    </>
  );
};

export default AppRoutes;