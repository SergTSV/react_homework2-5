import React from "react";
import store from "../../../redux/storeConfig";
import Actions from "../Actions/Actions";
import Button from "../../Button/Button";
import { removeFromCart } from "../../../redux/cart/actions";
import Modal from "../Modal";
import { hideModalWindow } from "../../../redux/modals/actions";

export const removeModal = () => {
  const cart = store.getState().cart;
  const item = store.getState().popup.item;
  const quantity = cart.filter(el => el.vendorCode === item.vendorCode).length

  const props = () => {

    if (quantity > 0) {
      return {
        header: `Do you want to remove ${item.name} from cart?`,
        text: `You have ${quantity} items in cart`,
        closeButton: true,
        modifier: 'remove-file',
        actions: <Actions buttons={[
          <Button key="0"
            text="Yes"
            handler={() => store.dispatch(removeFromCart(item))}
            modifier="remove-file-modal" />,
          <Button key="1"
            text="No"
            handler={(ev) => store.dispatch(hideModalWindow(ev))}
            modifier="remove-file-modal close" />]}
        />
      }
    } else {
      return {
        header: "Items were successfully removed",
        text: `You have ${cart.length} other items in cart`,
        closeButton: true,
        modifier: 'remove-file',
        actions: <Actions buttons={[

          <Button key="2"
            text="Close"
            handler={(ev) => store.dispatch(hideModalWindow(ev))}
            modifier="remove-file-modal close" />]}
        />
      }
    }
  }
  const currentProps = props();

  return <Modal {...currentProps} />
}