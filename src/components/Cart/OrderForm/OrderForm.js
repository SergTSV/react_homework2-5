import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { Formik, Form, Field } from "formik";
import * as yup from 'yup'
import "yup-phone";
import './OrderForm.scss'
import Button from "../../Button/Button";
import { createOrder, setConsumer } from "../../../redux/cart/order/actions";
import { selector } from "../../../redux/cart/selector";
import getItems from "../../../utils/getUniqueProductsArray";


const OrderForm = ({ setConsumer,
  createOrder,
  cart,
}) => {

  const consumerSchema = yup.object().shape({
    name: yup
      .string()
      .required(),
    surname: yup
      .string()
      .required(),
    age: yup
      .number()
      .required()
      .positive()
      .integer(),
    address: yup
      .string()
      .required(),
    phone: yup
      .string()
      .required()
      .min(9)
      .max(10)
  });

  return <Formik
    initialValues={{
      name: "",
      address: "",
      phone: "",
      surname: "",
      age: ""
    }}
    validationSchema={consumerSchema}
    validateOnChange={false}
    validateOnBlur={false}
    onSubmit=
    {(values, { setSubmitting, resetForm }) => {
      const items = getItems(cart),
        consumer = values
      createOrder({ consumer, items });
      console.log('consumer:', consumer, '\n', 'items:\n', items)
      localStorage.setItem('cart', JSON.stringify([]))
      setSubmitting(false);
      resetForm();
    }}
  >
    {({ errors, isSubmitting }) => {

      return <Form className="order-form">

        <label className="order-form__label" htmlFor="name">name:</label>
        <Field
          type="text"
          name="name"
          onBlur={({ target }) => setConsumer({ name: target.value })}
        />
        <div className="order-form__validation-field">
          {errors.name && <span className="error-msg">{errors.name}</span>}
        </div>

        <label htmlFor="surname">surname:</label>
        <Field
          type="text"
          name="surname"
          onBlur={({ target }) => setConsumer({ surname: target.value })}
        />
        <div className={"order-form__validation-field"}>
          {errors.surname && <span className="error-msg">{errors.surname}</span>}
        </div>

        <label htmlFor="address">delivery address:</label>
        <Field
          type="text"
          name="address"
          onBlur={({ target }) => setConsumer({ address: target.value })}
        />
        <div className={"order-form__validation-field"}>
          {errors.address && <span className="error-msg">{errors.address}</span>}
        </div>

        <label htmlFor="phone">phone number:</label>
        <Field
          type="tel"
          name="phone"
          placeholder="+XXX-XXX-XX-XX"
        //onBlur={({ target }) => setConsumer({ phone: target.value })}
        />
        <div className={"order-form__validation-field"}>
          {errors.phone && <span className="error-msg">{errors.phone}</span>}
        </div>

        <label htmlFor="age">age:</label>
        <Field
          type="text"
          name="age"
          onBlur={({ target }) => setConsumer({ age: target.value })}
        />
        <div className="order-form__validation-field">
          {errors.age && <span className="error-msg">{errors.age}</span>}
        </div>

        <Button type="submit"
          text="checkout"
          modifier={isSubmitting ? 'form-submitting' : 'form'}
          disabled={isSubmitting} />
      </Form>
    }}
  </Formik>
}

const mapStoreToProps = store => ({
  cart: selector(store),
})

const mapDispatchToProps = dispatch => ({
  createOrder: (consumerData) => dispatch(createOrder(consumerData)),
  setConsumer: (data) => dispatch(setConsumer(data)),
});

export default connect(mapStoreToProps, mapDispatchToProps)(OrderForm);


OrderForm.propTypes = {
  setConsumer: PropTypes.func.isRequired,
  createOrder: PropTypes.func.isRequired,
  cart: PropTypes.array.isRequired
}

OrderForm.defaultProps = {
  cart: []
}