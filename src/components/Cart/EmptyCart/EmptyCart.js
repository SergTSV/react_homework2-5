import React from 'react';
import './EmtyCart.scss'

const EmptyCart = () => {
  return (
    <div className='empty-cart'>
      <p className='empty-cart__text'>Your cart is empty now</p>
    </div>
  );
};

export default EmptyCart;