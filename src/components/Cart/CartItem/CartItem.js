import React from 'react';
import './CartItem.scss'
import Button from "../../Button/Button";
import PropTypes from 'prop-types';

function CartItem(props) {
  const { image, vendorCode, name, scale, price, quantity, removeAction } = props;
  const sum = price * quantity;

  return (
    <div className="cart-item">
      <div className="cart-item__image"><img src={image} alt={`product code: ${vendorCode}`} /></div>
      <h4 className="cart-item__title">{name}</h4>
      <div className="cart-item__description">
        <span className="cart-item__code">{`vendor code: ${vendorCode}`}</span>
        <span className="cart-item__color">{`scale: ${scale}`}</span>
        <span className="cart-item__color">{`quantity: ${quantity}`}</span>

        <span className="cart-item__price">{"sum: "}
          <span className="cart-item__price-sum">{sum} &#36;
          </span>
        </span>

        <Button modifier="cart-remove" handler={() => removeAction({ ...props })}
          element={<span role="img" aria-label="cross" >&#10060;</span>} />

      </div>
    </div>
  );
}

export default CartItem;

CartItem.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  vendorCode: PropTypes.number.isRequired,
  scale: PropTypes.string.isRequired,
  quantity: PropTypes.number.isRequired,
  removeAction: PropTypes.func.isRequired
}