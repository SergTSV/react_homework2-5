import React from 'react';
import Catalog from "../../components/Catalog/Catalog";
import store from "../../redux/storeConfig";
import './Favorites.scss'


const Favorites = () => {

  const favorites = store.getState().favorites
    || JSON.parse(localStorage.getItem('favorites'))

  if (favorites.length === 0) {
    return (
      <div className='fav-page'>
        <p className="fav-page__empty-favorites-msg">No items added to favorites</p>
      </div>
    )

  } else {
    return (
      <Catalog renderedFavorites={favorites} selector='favorites' />
    )
  }
}

export default Favorites;