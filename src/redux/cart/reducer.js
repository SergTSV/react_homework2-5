import initialStore from "../initalStore";
import { CART_ADD, CART_CLEAR, CART_REMOVE } from "../actionTypes";

export default function (cart = initialStore.cart, { type, item = null }) {
  switch (type) {
    case CART_ADD:
      const newCart = [...cart, item];
      localStorage.setItem('cart', JSON.stringify(newCart))
      return newCart;

    case CART_REMOVE:
      const currentCart = [...cart],
        itemToRemove = currentCart.find(el => el.vendorCode === item.vendorCode);
      currentCart.splice(currentCart.indexOf(itemToRemove), 1);
      localStorage.setItem('cart', JSON.stringify(currentCart));

      return currentCart;

    case CART_CLEAR:
      return initialStore.cart;

    default: return cart
  }
}