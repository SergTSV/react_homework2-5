const initialStore = {
  popup: {
    name: null,
    item: {
      name: null,
      vendorCode: null,
      price: null,
      scale: null,
      image: null,
    }
  },
  goods: [],
  favorites: JSON.parse(localStorage.getItem('favorites')) || [],
  cart: JSON.parse(localStorage.getItem('cart')) || [],
  order: {
    items: [],
    consumer: {
      name: "",
      address: "",
      phone: "",
      surname: "",
      age: ""
    }
  }
}

export default initialStore;