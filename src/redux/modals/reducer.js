import intialStore from "../initalStore";
import { MODAL_ADD_TO_CART, MODAL_HIDE, MODAL_REMOVE_FROM_CART } from "../actionTypes";


export default function (popup = intialStore.popup, action) {
  switch (action.type) {
    case MODAL_ADD_TO_CART:
      return {
        ...popup,
        name: 'add-to-cart',
        item: { ...action.item },
      }

    case MODAL_REMOVE_FROM_CART:
      return {
        ...popup,
        name: 'remove-from-cart',
        item: { ...action.item },
      }

    case MODAL_HIDE:
      if (action.class === 'modal' || action.class === 'close-btn' || action.class.includes('close')) {
        return intialStore.popup;
      } else {
        return popup;
      }

    default: return popup;
  }
}