import initialStore from "../initalStore";
import { FAVORITES_TOGGLE } from "../actionTypes";

export default function (favorites = initialStore.favorites, { type, payload }) {
  switch (type) {
    case FAVORITES_TOGGLE:

      if (!favorites.some(el => el.vendorCode === payload.vendorCode)) {
        const newFavorites = [...favorites, payload]
        localStorage.setItem('favorites', JSON.stringify(newFavorites));
        return newFavorites;

      } else {
        const newFavorites = [...favorites],
          itemToRemove = newFavorites.find(el => el.vendorCode === payload.vendorCode);

        newFavorites.splice(newFavorites.indexOf(itemToRemove), 1)
        localStorage.setItem('favorites', JSON.stringify(newFavorites));
        return [...newFavorites]
      }

    default: return favorites
  }
}