import {applyMiddleware, createStore} from "redux";
import initialStore from './initalStore'
import reducer from './rootReducer'
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";



const store = createStore(
  reducer,
    initialStore,
  composeWithDevTools(applyMiddleware(thunk))
);

export default store;