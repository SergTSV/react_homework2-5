export default function (skuArray) {
  const duplicate = skuArray.map(item => {
    item.quantity = skuArray.filter(el => el.vendorCode === item.vendorCode).length
    return item;
  });
  const codes = [...new Set(duplicate.map(item => item.vendorCode))]

  return codes.map(code => duplicate.find(item => item.vendorCode === code));
}